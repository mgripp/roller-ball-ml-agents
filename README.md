# Machine Learning mit Unity: ML-Agents

Referenzen
- [Unity Basics (für alle, die noch nie mit Unity gearbeitet haben)](https://learn.unity.com/project/roll-a-ball-tutorial)
- [ML-Agents Dokumentation](https://github.com/Unity-Technologies/ml-agents/tree/latest_release/docs/)

## Was ist ML-Agents?

ML-Agents ist ein Framework für Unity, das es ermöglicht, Modelle für Agenten in einer Unity-Spielumgebung zu trainieren und sie dann mit dem daraus entstandenen Modell zu steuern. Das Framework hat inzwischen recht viele Funktionen, z.B.

- Steuerung von Agenten anhand von beliebigen Umgebungsdaten, auch rohe Bilddaten einer Kamera
- Steuerung von Agenten mit unterschiedlichen Teamzugehörigkeiten, Agenten können gegeneinander spielen
- Recurrent Networks, Long Short Time Memory
- Training aus Ausführung (Inference Engine) von Modellen an zahlreichen Agenten in derselben Szene
- ...


## Beispiele

- [ML-Agents Deep Learning Race Driver](https://youtu.be/gEf9V03HWv0?t=97)
- [ML-Agents Survival Shooter](https://youtu.be/qjA8sjCezRQ?t=305)
- [ML-Agents AI Jet Learns to Bomb Missile Launcher](https://youtu.be/iP28kOCpW94?t=308)
- [OpenAI Hide and Seek](https://www.youtube.com/watch?v=kopoLzvh5jY)


## Wie funktioniert ML-Agents?

- **Python API**: Hier passieren alle wesentlichen Machine-Learning-Algorithmen
  - Tensorflow
  - bekommt bei Training alle Obversavtions von Agenten
  - erzeugt daraus während das Training alle Entscheidungen für Agenten
  - trainierte Modelle werden exportiert und sind dann ohne die API verwendbar
- **Learning Environment**: Unity
  - **Agents**: Einer je Spielerfigur in der Szene
  - **Brain/Policy**: Eine je Typ von Agent (Verhaltensweisen)
    - hier laufen Beobachtungen aller Agenten zusammen
    - getroffenen Entscheidungen kommen von hier zu allen Agenten
  - **Academy**: Immer eine
    - verwaltet Kommunikation mit Python API (**External Communicator**)
    - sorgt dafür, dass Agenten bei Training synchron laufen

![architecture](https://raw.githubusercontent.com/Unity-Technologies/ml-agents/latest_release/docs/images/learning_environment_example.png)

Agenten nehmen Umgebung durch Observations wahr. Das kann jeden beliebige Zahleneingabe sein, z.B. die Position eines Objekts, die Distanz zu einem Objekt oder ein Winkel. Möglich sind auch "Taster", die strahlenartig von dem Agent weggehen (Raycast Observation).

![raycast observation](https://raw.githubusercontent.com/Unity-Technologies/ml-agents/master/docs/images/ray_perception.png)

Es sind auch Visual Observations durch eine Kamera bzw. eine Render Texture möglich.

![visual observation](https://raw.githubusercontent.com/Unity-Technologies/ml-agents/master/docs/images/visual-observation-rawimage.png)


![visual observation ingame](https://raw.githubusercontent.com/Unity-Technologies/ml-agents/master/docs/images/gridworld.png)

- Alle Obsrevations dienen als Inputs für ein neuronales Netzwerk (n-dimensionales Array von floats)
- neuronale Netzwerke liegen normalerweise im Barracuda-Format (.nn) vor (daher ist das Barracuda Package in Unity erforderlich für ML-Agents)
- Ausgaben des Netzwerks sind sog. Actions, ein eindimensionales Array von floats
  - kann z.B. als Steuerung für Bewegung auf x- und y-Achse verwendet werden


## Unity vorbereiten

- Unity starten
- Neues Projekt starten
- Edit > Project Setting > Player
  - für alle Plattformen:
    - *Other Settings* ausklappen
    - *Scripting Runtime Version* auf *Experimental (.NET 4.6 Equivalent or .NET 4.x Equivalent)* setzen
- ML-Agents Package importieren:
  - Package Manager in Unity öffnen: Windows > Package Manager
  - Neues Package hinzufügen: Add package from disk...
    - zu finden im geklonten Repository: ml-agents/com.unity.ml-agents/package.json
- Projekt sichern


## Szene einrichten


**1. 3D-Objekt für Boden erzeugen: *Plane***

  - Name: Floor
  - Transform: Position auf (0, 0, 0)
  - Textur anlegen: Assets/Textures/...
    - z.B. aus [Google Bilder](https://www.cheetah3d.com/forum/index.php?attachments/34367/)
  - Material anpassen
    - Neu erzeugen unter Assets/Materials/Floor.mat
    - Als Material, Element 0 für Mesh-Renderer auswählen
    - Zuvor erzeugte Textur als Albedo-Kanal einrichten
    - (Farbe dunkler machen)
    - (Tiling erhöhen)
    - Smoothness auf 0 setzen
  - Rigidbody-Component hinzufügen
    - Is Kinematic: true (Die Plane bleibt an Ort und Stelle, verhält sich sonst aber physikalisch korrekt)


**2. 3D-Objekt für Spielerfigur (Ball) erzeugen: *Sphere***

  - Name: BallAgent
  - Transform: Position auf (0, 0.5, 0)
  - Material anpassen:
    - Neu erzeugen unter Assets/Materials/Checker.mat
    - Als Material, Element 0 für Mesh-Renderer auswählen
    - Zuvor erzeugte Textur als Albedo-Kanal einrichten
    - Smoothness auf 0 setzen
  - Rigidbody-Component hinzufügen

**3. 3D-Objekt erzeugen: *Cube***

  - Name: Target
  - Transform: Position auf (..., 0.5, ...)
  - Material anpassen
    - Neu erzeugen unter Assets/Materials/Target.mat
    - Als Material, Element 0 für Mesh-Renderer auswählen
    - Farbe wählen
    - Smoothness auf 0 setzen
  - Rigidbody-Component hinzufügen



## Spielmechanik integrieren

**Ziel:**
- Der Spieler kann Ball in vier Richtungen rollen lassen (rechts, links, oben, unten)
- Das Spiel ist gewonnen, sobald der Ball das Target berührt, die Runde endet
- Das Spiel ist verloren, sobald der Ball von der Plattform fällt, die Runde endet
- bei Rundenende:
  - Target bekommt eine neue zufällige Position
  - Ball wird wieder auf die Startposition gesetzt
  - Eine neue Runde beginnt



### Kamera besser platzieren

z.B.:
- Position: (0, 5.2, -12)
- Rotation: (16, 0, 0)


### Ball rollen lassen

- Neues C# Skript erzeugen: Assets/Scripts/BallAgent.cs
- Muss namespace *'MLAgents'* verwenden
- Klasse erbt von *'Agent'*
  - Subklasse von *'Monobehaviour'*
  - Enthält alle wesentlichen Funktionen für einen mit ML-Agents trainierbaren Agent
- *'Start'* und *'Update'* werden nicht benötigt

- Neue Funktion: *'public override void OnActionReceived(float[] vectorAction)'*
  - Funktion, die ML-Agents immer aufruft, wenn neue Aktionen durch menschliche Eingaben oder solche von einem neuronalem Netzwerk bzw. Brain eingehen (meistens alle paar Frames bzw. FixedUpdates = 0.02s)
  - Hier soll folgendes passieren:
    - Aktionen aus *'vectorAction'*-Array auslesen und umsetzen
    - Prüfen, ob Target erreicht wurde und Spiel ggf. zurücksetzen
    - Prüfen, ob Agent von der Plattform gefallen ist und Spiel ggf. zurücksetzen

- Neue Funktion: *'public override void Heuristic(float[] actionsOut)'*
  - Ermöglicht Befehle für den Fall, das keine Steuerung des Agenten durch ein neuronales Netzwerk bzw. Brain erfolgt
  - Hier soll folgendes passieren:
    - Tastatureingaben des Anwenders auslesen
    - Als Aktionen an *'OnActionReceived'* weitergeben
  - ML-Agents bzw. die Academy legt von außen das Array 'actionsOut' an (Größe festgelegt druch Action Size in 'Behaviour Parameters') und ruft unsere *'Heuristic'*-Funktion auf, gibt das Array an diese und leitet das dann mit Werten gefüllte Array später weiter an *'OnActionReceived'*!

- Skript als Komponente zu BallAgent-Objekt hinzufügen in der Szene
  - Speed setzen auf z.B. 10.0
  - Target setzen
  - Rigidbody setzen
  - Autotmatisch wird auch eine Komponente *'Behaviour Parameters'* ergänzt
    - dient der Einstellung des Verhaltens und der Ein- und Ausgabewerte des verwendeten Brains
    - Parameter einstellen:
      - Behaviour Name: RollerBall
      - Vector Action:
        - Space Type: Continuous (Fließkommawerte)
        - Space Size: 2?
        - **Warum?**
          - Aktionen beschränken sich auf die horizontale und die vertikale Achse, also zwei floats von -1.0 bis 1.0 (zwei kontinuierliche Werte)
      - Behaviour Type: Heuristic Only
  - Komponente *'Decision Requester'* hinzufügen
    - Erfragt in regelmäßigen Zeitabständen Entscheidungen vom Brain bzw. der Heuristik
    - Decision Period: Intervall einstellbar
    - Take Actions Between auf true
      - Einstellbar, ob Aktionen zwischen den einzelnen Entscheidungen fortgeführt werden
    - Decision Period auf 5 setzen (jedes FixedUpdate * 5, sprich alle 0.02 * 5 = 0.1 Sekunden)

```
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;


public class BallAgent : Agent
{
    [SerializeField] Transform target;
    [SerializeField] Rigidbody rigidbody;
    [SerializeField] float speed;

    // similair to Update()
    public override void OnActionReceived(float[] vectorAction)
    {
        // actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = vectorAction[0];
        controlSignal.z = vectorAction[1];
        rigidbody.AddForce(controlSignal * speed);

        // distance to target
        float distanceToTarget = Vector3.Distance(
            this.transform.localPosition,
            target.localPosition
        );

        // die getrennte Aufteilung der beiden Episodenenden wird später noch wichtig!

        // reached target
        if (distanceToTarget < 1.42f)
        {
            EndEpisode();
        }

        // fell from platform
        if (this.transform.localPosition.y < 0)
        {
            EndEpisode();
        }
    }

    // used for human player input
    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = Input.GetAxis("Horizontal");
        actionsOut[1] = Input.GetAxis("Vertical");
    }
}
```

### Spiel zurücksetzen

- Neue Funktion: *'public override void OnEpisodeBegin()'*
  - wird von ML-Agents am Ende einer Trainings-Episode aufgerufen, sprich immer bei dem Aufruf EndEpisode()
  - Muss folgendes bewirken:
    - Ball wieder auf Startposition umsetzen
    - Ball-Geschwindigkeit wieder auf 0, wenn er heruntergefallen ist
    - Target auf neue zufällige Position setzen

```
...

public class BallAgent : Agent
{
    ...

    //Similar to Start(), also used for resetting
    public override void OnEpisodeBegin()
    {
        if (this.transform.localPosition.y < 0)
        {
            // If the Agent fell, zero its momentum
            this.rigidbody.angularVelocity = Vector3.zero;
            this.rigidbody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(0, 0.5f, 0);
        }

        // Move the target to a new spot
        target.localPosition = new Vector3(Random.value * 8 - 4,
                                           0.5f,
                                           Random.value * 8 - 4);
    }

    ...
}
```


### Agent durch Brain steuern

- Neue Funktion anlegen: *'public override void CollectObservations(VectorSensor sensor)'*
  - ML-Agents speist darüber Daten über die Umgebung des Agents ein
  - welche Daten das genau sind, legen wir fest: **Was ist sinnvoll für unseren Agenten zu wissen?**
  - Wir nutzen diese Informationen:
    - Position des Targets (x- und z-Koordinate)
    - Position des Balls (x- und z-Koordinate)
    - Geschwindigkeit des Balls (x- und z-Richtung)
    - **y-Richtung nicht relevant!**
  - Alle Daten werden in ein float-Array geschrieben
    - **Wie lang wird dieses Array immer sein?**  
     => 6!

 ```
 ...

 public class BallAgent : Agent
 {
     ...

     /**
     * The Agent sends the information we collect to the Brain, which uses it to make a decision.
     * When you train the Agent (or use a trained model), the data is fed into a neural network as
     * a feature vector.
    **/
    public override void CollectObservations(VectorSensor sensor)
    {
        // Target positions
        sensor.AddObservation(target.localPosition.x);
        sensor.AddObservation(target.localPosition.z);

        // Agent positions
        sensor.AddObservation(transform.localPosition.x);
        sensor.AddObservation(transform.localPosition.z);

        // Agent velocity
        sensor.AddObservation(rigidbody.velocity.x);
        sensor.AddObservation(rigidbody.velocity.z);
    }

     ...
 }
 ```

- *'Vector Observation'* von *'Behaviour Parameters'* anpassen:
  - Space Type: Continuous (sollte schon zuvor so eingestellt worden sein)
  - Space Size: 6
- Behaviour Type: Default


### Modell für Agent trainieren (Reinforcement Learning)

[Reinforcement Learning in Unity aus der ML-Agents Dokumentation](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Learning-Environment-Design.md)


- Rewards vergeben (0.0-1.0), wenn Target erreicht wurde in dieser Episode
  - SetReward(1.0f) in *'OnActionReceived'* ergänzen

```
...

public class BallAgent : Agent
{
    ...

    public override void OnActionReceived(float[] vectorAction)
    {
        // actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = vectorAction[0];
        controlSignal.z = vectorAction[1];
        rigidbody.AddForce(controlSignal * speed);

        // distance to target
        float distanceToTarget = Vector3.Distance(
            this.transform.localPosition,
            target.localPosition
        );

        // reached target
        if (distanceToTarget < 1.42f)
        {
            SetReward(1.0f);
            EndEpisode();
        }

        // fell off platform
        if (this.transform.localPosition.y < 0)
        {
            EndEpisode();
        }
    }

    ...
}
```

- Config anlegen:
  - RollerBall.yaml
  - Als Vorlage kann die config aus dem ml-agents Ordner dienen: *ml-agents/config/trainer_config.yaml*
  - Hier können diverse Parameter eingestellt werden, wie z.B. Speicherort von Modell und Logs, außerdem Hyperparameter für das Training
  - Jeweils ein Dict mit dem Namen des Brains als Key beinhält Einstellungen für das Training des zugehörigen Modells
  - 'default'-Eintrag enthält Standtart-Einstellungen, die durch spezifische Einstellungen des trainierten Brains überschrieben werden
  - Eingestellte Verzeichnisse für *'model_path'* und *'summary_path'* müssen angelegt werden

```
default:
    ...

RollerBall:
    trainer: ppo
    model_path: ./models/RollerBall
    summary_path: ./summaries/RollerBall
    hidden_units: 128
    learning_rate: 0.0003
    batch_size: 10
    buffer_size: 100
    summary_freq: 1000
    max_steps: 4.0e4
```


[Alle Konfigurationsmöglichkeiten in der ML-Agents Dokumentation](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Training-ML-Agents.md)

- Trainings-Sitzung starten durch Konsolenbefehl:
```
cd <Projektordner>
mlagents-learn ./RollerBall.yaml --run-id=RollerBall --train
```
- **./RollerBall.yaml:** Pfad zur Config
- **--run-id=RollerBall:** ID des trainierten Brains
- **--train:** Es wird trainiert, nicht nur simuliert

- Training wird gestartet:
  - Aktionen des Agenten werden in Unity simuliert (leicht beschleunigt)
  - ML-Agents im Hintergrund (Konsole) bearbeitet mit aus der Simulation gewonnenen Daten das Modell und speichert dieses bei Ende des Trainings
  - in der Konsole sind aktuelle Schritte am Ende eines jeden Batches zu sehen
  - Ende tritt ein, wenn:
    - Unity-Ausführung beendet wird
    - *'max_steps'* wird erreicht

### Training, wenn bereits ein Modell angelegt wurde

Wenn wir bereits zuvor einmal das Modell mit der angegebenen ID trainiert haben, wird ML-Agents verlangen, dass wir die Parameter '--resume' oder '--force' nutzen.

#### --resume

So wird der Trainer den letzten Checkpoint unserer Training an dem Modell mit der ID 'RollerBall' in dem models-Ordner fortsetzen.  
**Achtung:** Wir können unter Umständen kein Modell weitertrainieren, das mit einer älteren Version von ML-Agents trainiert wurde.

```
cd <Projektordner>
mlagents-learn ./RollerBall.yaml --resume --run-id=RollerBall --train
```

#### --force

So wird der Trainer das Modell aus unserem letzten Training an dem Modell mit der ID 'RollerBall' in dem models-Ordner überschreiben, ebenso seine Checkpoints.

```
cd <Projektordner>
mlagents-learn ./RollerBall.yaml --force --run-id=RollerBall --train
```

## Training beschleunigen

Das Training kann deutlich beschleunigt werden, indem mehr als ein Agent Erfahrungen sammelt für sein Brain:

#### Aufbau zu Prefab machen:
- leeres GameObject erzeugen mit Name: RollerBall
- alle Szenenobjekte außer Licht und Kamera als Kinder
- Objekt als Prefab einrichten: Prefabs/RollerBall.prefab
- sicherstellen, das Positionen im lokalen System noch stimmen!
- zahlreiche Duplikate des Prefabs anlegen (ca. 60)
- fertige .nn-Datei in das Projekt importieren, als Model für Agent festlegen in *'Behaviour Parameters'*


## Training überwachen mit Tensorboard

- zweites Konsoolenfenster öffnen und Befehle eingeben:
```
cd <Projektordner>
tensorboard --logdir=summaries --port=6006
```
- Browser öffnen mit Link: *http://localhost:6006*
- es öffnet sich eine Weboberfläche mit unterschiedlichen Graphen, die den aktuellen Trainingsstand wiedergeben
- [Bedeutung der aufgeführten Werte und weitere Informationen in der ML-Agents Dokumentation](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Using-Tensorboard.md)
