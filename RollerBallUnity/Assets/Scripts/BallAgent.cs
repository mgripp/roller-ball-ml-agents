// https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Learning-Environment-Create-New.md

using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;


public class BallAgent : Agent
{
    [SerializeField] Transform target;
    [SerializeField] Rigidbody rigidbody;
    [SerializeField] float speed;

    //Similar to Start(), also used for resetting
    public override void OnEpisodeBegin()
    {
        if (this.transform.localPosition.y < 0)
        {
            // If the Agent fell, zero its momentum
            this.rigidbody.angularVelocity = Vector3.zero;
            this.rigidbody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(0, 0.5f, 0);
        }

        // Move the target to a new spot
        target.localPosition = new Vector3(Random.value * 8 - 4,
                                           0.5f,
                                           Random.value * 8 - 4);
    }

    /**
     * The Agent sends the information we collect to the Brain, which uses it to make a decision.
     * When you train the Agent (or use a trained model), the data is fed into a neural network as
     * a feature vector.
    **/
    public override void CollectObservations(VectorSensor sensor)
    {
        // Target positions
        sensor.AddObservation(target.localPosition.x);
        sensor.AddObservation(target.localPosition.z);

        // Agent positions
        sensor.AddObservation(transform.localPosition.x);
        sensor.AddObservation(transform.localPosition.z);

        // Agent velocity
        sensor.AddObservation(rigidbody.velocity.x);
        sensor.AddObservation(rigidbody.velocity.z);
    }

    // similair to Update()
    public override void OnActionReceived(float[] vectorAction)
    {
        // actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = vectorAction[0];
        controlSignal.z = vectorAction[1];
        rigidbody.AddForce(controlSignal * speed);

        // distance to target
        float distanceToTarget = Vector3.Distance(
            this.transform.localPosition,
            target.localPosition
        );

        // reached target
        if (distanceToTarget < 1.42f)
        {
            SetReward(1.0f);
            EndEpisode();
        }

        // fell from platform
        if (this.transform.localPosition.y < 0)
        {
            EndEpisode();
        }
    }

    // used for human player input
    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = Input.GetAxis("Horizontal");
        actionsOut[1] = Input.GetAxis("Vertical");
    }
}
